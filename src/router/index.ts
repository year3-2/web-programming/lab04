import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/my-files-view",
      name: "My file view",
      component: () => import("../views/MyFileView.vue"),
    },
    {
      path: "/Shared-with-me-view",
      name: "shared with me view",
      component: () => import("../views/ShareWithMe.vue"),
    },
    {
      path: "/Starred-view",
      name: "Starred view",
      component: () => import("../views/StarredView.vue"),
    },
  ],
});

export default router;
